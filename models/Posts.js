/*jslint node: true*/
var mongoose = require('mongoose');

var PostSchema = new mongoose.Schema({
  title: 'String',
  content: 'String',
  datePosted: 'Date'
});

mongoose.model('Post', PostSchema);