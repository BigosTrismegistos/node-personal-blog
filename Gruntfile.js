/*jslint node: true*/
module.exports = function(grunt) {
  
  var FE_JS = [
    'public/javascripts/blog/blog.js',
    'public/javascripts/blog/**/*.js'
  ];
  
  var BE_JS = [
    'app.js',
    'models/**/*.js',
    'routes/**/*.js',
    'views/**/*.js'
  ];
  
  var DST_DIR = 'public/javascripts/dist/';
  
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    jslint: {
      server: {
        src: BE_JS,
        directives: {
          node: true,
          nomen: true,
          indent: 2,
          unparam: true
        }
      },
      client: {
        src: FE_JS,
        directives: {
          browser: true,
          nomen: true,
          indent: 2,
          unparam: true
        }
      }
    },
    concat: {
      options: {
        separator: '\n'
      },
      dist: {
        src: FE_JS,
        dest: DST_DIR + 'blog.js'
      }
    },
    uglify: {
      build: {
        src: '<%= concat.dist.dest %>',
        dest: DST_DIR + 'blog.min.js'
      }
    },
    express: {
      dev: {
        options: {
          script: './bin/www',
          background: false          
        }
      }
    },
    watch: {
      files: FE_JS.concat(BE_JS),
      tasks: ['build']
    }
  });
  
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-jslint');
  grunt.loadNpmTasks('grunt-express-server');
  
  grunt.registerTask('build', ['jslint', 'concat', 'uglify']);
  grunt.registerTask('startDev', ['build', 'express', 'watch']);
};  