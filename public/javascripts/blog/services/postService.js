/*global angular*/
angular.module('blog').factory('posts', ['rest', '$q', function (rest, $q) {
  "use strict";
  var getLatestFn = function () {
    return $q(function (resolve, reject) {
      rest.get('/posts')
        .success(function (data) {
          resolve(data);
        })
        .error(function (data, status) {
          reject(status);
        });
    });
  };
  return {
    getLatest: getLatestFn
  };
}]);