/*global angular*/
angular.module('blog').factory('auth', ['$q', function ($q) {
  "use strict";
  return $q(function (resolve) {
    resolve(true);
  });
}]);