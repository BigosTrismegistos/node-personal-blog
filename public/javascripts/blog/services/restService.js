/*global angular*/
angular.module('blog').factory('rest', ['$http', function ($http) {
  "use strict";
  var getFn = function (url) {
    return $http.get(url);
  };
  return {
    get: getFn
  };
}]);