/*global angular*/
angular.module('blog', ['ngMaterial', 'ui.router'])
  .config(['$urlRouterProvider', '$stateProvider', '$mdThemingProvider', function ($urlRouterProvider, $stateProvider, $mdThemingProvider) {
    "use strict";
    $stateProvider
      .state('blog', {
        templateUrl: '/partials/main.html',
        url: '/',
        abstract: true,
        resolve: {
          auth: 'auth'
        }
      })
      .state('blog.postlist', {
        templateUrl: '/partials/postlist.html',
        url: '',
        controller: 'PostListCtrl'
      });
    $urlRouterProvider.otherwise('/');
    $mdThemingProvider.theme('default')
      .primaryPalette('indigo')
      .accentPalette('teal')
      .warnPalette('orange');
  }]);