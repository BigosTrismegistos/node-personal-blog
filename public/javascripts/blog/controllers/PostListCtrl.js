/*global angular*/
angular.module('blog').controller('PostListCtrl', ['$scope', 'posts', function ($scope, posts) {
  "use strict";
  posts.getLatest().then(function success(psts) {
    $scope.posts = psts;
  });
}]);